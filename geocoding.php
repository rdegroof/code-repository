<html>
<head>
</head>
<body onLoad="">
<h2>Cached response:</h2>
<pre>Array
(
    [lon] => -106.374
    [lat] => 39.6403
)
</pre>

<h2>Full response:</h2>
<pre>stdClass Object
(
    [results] => Array
        (
            [0] => stdClass Object
                (
                    [address_components] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [long_name] => Vail
                                    [short_name] => Vail
                                    [types] => Array
                                        (
                                            [0] => locality
                                            [1] => political
                                        )

                                )

                            [1] => stdClass Object
                                (
                                    [long_name] => Eagle
                                    [short_name] => Eagle
                                    [types] => Array
                                        (
                                            [0] => administrative_area_level_2
                                            [1] => political
                                        )

                                )

                            [2] => stdClass Object
                                (
                                    [long_name] => Colorado
                                    [short_name] => CO
                                    [types] => Array
                                        (
                                            [0] => administrative_area_level_1
                                            [1] => political
                                        )

                                )

                            [3] => stdClass Object
                                (
                                    [long_name] => United States
                                    [short_name] => US
                                    [types] => Array
                                        (
                                            [0] => country
                                            [1] => political
                                        )

                                )

                        )

                    [formatted_address] => Vail, CO, USA
                    [geometry] => stdClass Object
                        (
                            [bounds] => stdClass Object
                                (
                                    [northeast] => stdClass Object
                                        (
                                            [lat] => 39.652294
                                            [lng] => -106.279747
                                        )

                                    [southwest] => stdClass Object
                                        (
                                            [lat] => 39.6146279
                                            [lng] => -106.440773
                                        )

                                )

                            [location] => stdClass Object
                                (
                                    [lat] => 39.6402638
                                    [lng] => -106.3741955
                                )

                            [location_type] => APPROXIMATE
                            [viewport] => stdClass Object
                                (
                                    [northeast] => stdClass Object
                                        (
                                            [lat] => 39.652294
                                            [lng] => -106.279747
                                        )

                                    [southwest] => stdClass Object
                                        (
                                            [lat] => 39.6146279
                                            [lng] => -106.440773
                                        )

                                )

                        )

                    [types] => Array
                        (
                            [0] => locality
                            [1] => political
                        )

                )

        )

    [status] => OK
)
</pre>
<br /><br />
Code to generate this page:
<div style="background-color:#ccc; border:1px red solid; padding:10px;">
<p>&lt;?php<br>
include_once(&quot;../include/GoogleMap.php&quot;);<br />include_once(&quot;../include/JSMin.php&quot;);</p>
<p>$MAP_OBJECT = new GoogleMapAPI();
$MAP_OBJECT->_minify_js = isset($_REQUEST["min"])?FALSE:TRUE;<br />//setDSN is optional<br>
$MAP_OBJECT-&gt;setDSN(&quot;mysql://user:password@localhost/db_name&quot;);<br>
$geocodes = $MAP_OBJECT-&gt;getGeoCode(&quot;Vail, CO&quot;);<br>
$geocodes_full = $MAP_OBJECT-&gt;geoGetCoordsFull(&quot;Vail, CO&quot;);</p>
<p>?&gt;<br>
&lt;html&gt;<br>
&lt;head&gt;<br>
&lt;/head&gt;<br>
&lt;body onLoad=&quot;&quot;&gt;<br>
&lt;h2&gt;Cached response:&lt;/h2&gt;<br>
&lt;pre&gt;&lt;?=print_r($geocodes,true)?&gt;&lt;/pre&gt;</p>
<p>&lt;h2&gt;Full response:&lt;/h2&gt;<br>
&lt;pre&gt;&lt;?=print_r($geocodes_full,true)?&gt;&lt;/pre&gt;</p>
<p><br>
&lt;/body&gt;<br>
&lt;/html&gt;</p>
</div>
<script type="text/javascript">
	var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
	document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
	</script>
	<script type="text/javascript">
	try {
	var pageTracker = _gat._getTracker("UA-15982877-1");
	pageTracker._setDomainName(".bradwedell.com");
	pageTracker._trackPageview();
	} catch(err) {}</script>
</body>
</html>
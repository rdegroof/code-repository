<?php

// purpose: query the database for broadcast and ship data based on posted lat, lon, month, day, year, hour and duration

$link = mysqli_connect("localhost","rdegroof","vB784os3412","test") or die("Error " . mysqli_error($link));
$lat = $_POST['lat'];
$lon = $_POST['lon'];
$month = $_POST['month']; //get posted variables
$day = $_POST['day'];
$year = $_POST['year'];
$hour = $_POST['hour'];
$hourDur = $_POST['duration'];
//database query
$query = "SELECT b.mmsi as mmsi, s.name as name, b.lat as lat, b.lon as lon, b.date as date, b.time as time from broadcast b join ships s on s.mmsi = b.mmsi where lat >= $lat - 5 AND lat <= $lat+5 and lon >= $lon-5 and lon <= $lon+5 order by b.mmsi" or die("Error in the consult.." . mysqli_error($link));
	$result = $link->query($query);
	$array;
	unset($array);
	$array23 = array(array(array()));
	
	$counter = -1;
	$numVals = 0;
	$mmsiStore = -1;
	$valueArray = array(array());
	$values;
	$usedValue = true;
	while($row = mysqli_fetch_array($result)) {
		$mmsi = $row['mmsi']; //store mmsi so that entries can be separated
		
		
		if($mmsi !== $mmsiStore){ //cycle through array if this is a new set of mmsi
			
			if($usedValue){
				$counter++;
				$mmsiStore = $mmsi;
			}
			
			$numVals = 0;
			
			
			unset($valueArray);
			$valueArray[] = array(array());
			
			
		}
		$usedValue = false;
		$name = $row['name'];
		
		$latData = $row['lat'];
		
		$lonData = $row['lon'];
		
		$date = $row['date'];
		
		$time = $row['time'];
		
		$dateArray = explode("/", $date);
		
		$monthData = $dateArray[0];
		$dayData = $dateArray[1]; //get specific data from date data
		$yearData = $dateArray[2];
		
		$timeArray = explode(":", $time);
		
		$hourData = $timeArray[0];
		$minuteData = $timeArray[1]; //get specific time from time data
		$secondData = $timeArray[2];
		
		
		if($hourData <= $hour+$hourDur &&
		$hourData >= $hour &&
		$dayData == $day && //if value matches parameters
		$yearData == $year &&
		$monthData == $month){
			
			unset($values);
			
			$values = array();
			
			
			$array23[$counter][$numVals][] = $mmsi;
			$array23[$counter][$numVals][] = $name;
			$array23[$counter][$numVals][] = $latData; //store relevant data in array;
			$array23[$counter][$numVals][] = $lonData;
			$array23[$counter][$numVals][] = $date;
			$array23[$counter][$numVals][] = $time;
			$usedValue = true;
			
		$numVals++;	
		}
		
	}
	
	
	$x = 0;
	
	for($x = 0; $x < count($array23); $x++){
		echo "<fieldset>";
		echo "<legend>";
		echo $array23[$x][0][0];
		echo "</legend>";
		echo "<table border='1'>";
		echo "<th>MMSI</th><th>NAME</th><th>Lat</th><th>Lon</th><th>Date</th><th>Time</th>";
		
		$y = 0;
		
		for($y = 0; $y<count($array23[$x]); $y++){
			echo "<tr>";
				echo "<td>";
				echo $array23[$x][$y][0];
				echo "</td>";
				echo "<td>";
				echo $array23[$x][$y][1];
				echo "</td>";
				echo "<td>";
				echo $array23[$x][$y][2];
				echo "</td>";
				echo "<td>";
				echo $array23[$x][$y][3];
				echo "</td>";
				echo "<td>";
				echo $array23[$x][$y][4];
				echo "</td>";
				echo "<td>";
				echo $array23[$x][$y][5];
				echo "</td>";
			echo "</tr>";
			
		}
		
		echo "</table>";
		echo "</fieldset>";
		
	}
	
	

?>
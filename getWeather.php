<html>
 <head>
  <title>PHP Test 3</title>
 </head>
 <body>
 
<?php

?>

 
 
 <?php

//purpose: find relevant historical data by user defined lat/lon date/time

include 'simple_html_dom.php';
$link = mysqli_connect("localhost","rdegroof","vB784os3412","test") or die("Error " . mysqli_error($link));
//connect to database
set_time_limit(160); 
//time limit must be increased in case query takes too long


$lat = $_POST['lat'];
$lon = $_POST['lon']; //user defined variables
$latdir = $_POST['latdir'];
$londir = $_POST['londir'];

$query = "SELECT STATIONID, LAT, LON FROM stations where LAT >= ($lat-1) AND LAT <= ($lat+1) AND LON >= ($lon-1) AND
LON <= ($lon+1) AND LATDIR = '$latdir' AND LONDIR = '$londir'" or die("Error in the consult.." . mysqli_error($link));
//query for relevant stations
//execute the query.

$result = $link->query($query);


$year = $_POST['year'];
$month = $_POST['month']; //user defined variables
$day = $_POST['day'];
$hour = $_POST['hour'];
$stationval = "";
$retrievedLat = 0.0;
$retrievedLon = 0.0;
while($row = mysqli_fetch_array($result)) {
  $stationval = $row['STATIONID'];
$retrievedLat =$row['LAT']; //database reply
$retrievedLon = $row['LON'];	
$html = file_get_html('NDBC - Historical NDBC Data.htm'); 


echo $stationval;
echo ": ";
echo $year;
echo "<br />";
 if($html == null){
	echo "badfile";
 }
 $a = 0;
  
  foreach($html->find('ul') as $table){ //get stations for processing
	foreach($table->find('ul') as $innertable){
		foreach($innertable->find('li') as $p){
			$station = $p->plaintext;
			//echo ": ";
			$tok = strtok($station, ":");
			$flag = 0;
			if($tok == $stationval){
				$flag = 1;
				foreach($p->find('a') as $theLink){
				
					if($theLink->plaintext == $year){
						$historicaldatapage = file_get_html($theLink->href);
						$pagecounter = 0;
						$paragraphcnt = 0;
						foreach($historicaldatapage->find('table p') as $para){
							if($paragraphcnt++ == 2){
							
							
								foreach($para->find('a') as $pageLink){
									file_put_contents("Tmpfile.txt.gz", file_get_contents("http://www.ndbc.noaa.gov/data/historical/stdmet$pageLink->href"));
									$fp = gzopen("Tmpfile.txt.gz", "r"); //get compressed historical data and extract the file
									$weatherFile = gzread($fp, filesize("Tmpfile.txt.gz")*15);
									getWeatherData($weatherFile, $latdir, $londir, $link, $stationval, $retrievedLat, $retrievedLon); //process the data
									
									break;
								}
								break;
							}
							
						}
					}
					
				}
			
			}
			if($flag == 1){ // if this is not the right station
				break;
			}
			
			
			
			
			
			
			
		}
		if($flag == 1){  // if this is not the right station
			break;
		}
	
	}
	
  
  }
  echo "<br />";
  
  
} 


 
  

function getWeatherData($weatherData, $latdir, $londir, $link, $station, $retrievedLat, $retrievedLon){
	$year = $_POST['year'];
	$month = $_POST['month'];
	$day = $_POST['day']; //get user defined variables
	$hour = $_POST['hour'];
	$tok = strtok($weatherData, " "); //get first entry
		
	$dataTypeArray = array();
	$dataTypePointer = 0;
	$flagIsLower = false;
	$indexBreak = 0;
	while($tok != false){ //files are in different formats...this script detects which variables are present and stores them
		if(!ctype_alpha($tok)){
			if(is_numeric($tok)){
				$flagIsLower = true;
				
			
			}
			
			$string_case = "";
			$string_temp = $tok;
			while(strlen($string_temp) > 0){ //filter out lower case entries except hh, mm as these are unit definitions and not what we need
				$string_case = substr($string_temp, 0, 1);
				
				$string_temp = substr($string_temp, 1, strlen($string_temp)-1);
				if(ctype_alpha($string_case)){
					if(ctype_lower($string_case) && ($string_case !== 'h' && $string_case !== 'm')){
						$flagIsLower = true;
						
					}
				
				}
				
			
			}
			
		}
		else{
			if(ctype_lower($tok) && ($tok !== "hh" && $tok !== "mm")){
				$flagIsLower = true;
			}
			
		
		}
		if($flagIsLower){
				break;
		}
		echo $tok;
		$dataTypeArray[] = $tok;
		$tok = strtok(" \n\t");
		echo "PARSING: ";
		echo $tok;
		echo "<br />";
		$indexBreak++;
	}
	
	echo "Index Break: ";
	echo "$indexBreak";
	echo "<br />";
	$DataStoreArray = array();
	$BooleanArray = array();
	for($x = 0; $x < 16; $x++){
		$DataStoreArray[] = 0.0;
		$BooleanArray[] = false;
	
	}
	$indexMonth = 0;
	$indexDay = 0;
	$indexHour = 0;
	$indexMinute = 0;
	$indexWDIR = 0;
	$indexWSPD = 0;
	$indexGST = 0;
	$indexWVHT = 0;
	$indexDPD = 0;
	$indexAPD = 0;
	$indexMWD = 0;
	$indexPRES = 0;
	$indexATMP = 0;
	$indexWTMP = 0;
	$indexDEWP = 0;
	$indexVIS = 0;
	$indexTIDE = 0;
	$indexYear = 0;
	/*example file format
	
	#YY  MM DD hh mm WDIR WSPD GST  WVHT   DPD   APD MWD  ATMP  WTMP  DEWP  VIS  
	
	*/
	echo "Data Type array count: ";
	echo count($dataTypeArray);
	echo ": ";
	
	echo "<br />";
	for($x = 0; $x < count($dataTypeArray); $x++){
		$str = $dataTypeArray[$x];
		$str = preg_replace('/\s+/', '', $str);
		echo "IN COMPARISON: ";
		echo $str;
		
		if($str == "VIS"){
			echo "equal";
			echo "VIS";
			echo $str;
		}
		else{
			echo "not equal";
			echo $str;
			echo "VIS ";
		}
		
		echo "<br />";
		if($str == "#YY" || $str == "YY"){
			$indexYear = $x;
			$BooleanArray[0] = true;
		}
		else if($str == "MM"){
			$indexMonth = $x;
			$BooleanArray[1] = true;
		}
		else if($str == "DD"){
			$indexDay = $x;
			$BooleanArray[2] = true;
		}
		else if($str == "hh"){
			$indexHour = $x;
			$BooleanArray[3] = true;
		}
		else if($str == "mm"){
			$indexMinute = $x;
			$BooleanArray[4] = true;
		}
		else if($str == "WDIR" || $str == "WD"){
			$indexWDIR = $x;
			$BooleanArray[5] = true;
		}
		else if($str == "WSPD"){
			$indexWSPD = $x;
			$BooleanArray[6] = true;
		}
		else if($str == "GST"){
			$indexGST = $x;
			$BooleanArray[7] = true;
		}
		else if($str == "WVHT"){
			$indexWVHT = $x;
			$BooleanArray[8] = true;
		}
		else if($str == "DPD"){
			$indexDPD = $x;
			$BooleanArray[9] = true;
		}
		else if($str == "APD"){
			$indexAPD = $x;
			$BooleanArray[10] = true;
		}
		else if($str == "MWD"){
			$indexMWD = $x;
			$BooleanArray[11] = true;
		}
		else if($str == "ATMP"){ //store relevant variables
			$indexATMP = $x;
			$BooleanArray[12] = true;
		}
		else if($str == "WTMP"){
			$indexWTMP = $x;
			$BooleanArray[13] = true;
		}
		else if($str == "DEWP"){
			$indexDEWP = $x;
			$BooleanArray[14] = true;
		}
		else if($str == "VIS" || $str == "VIS "){
			echo "it is in VIS: ";
			echo $str;
			echo " : ";
			echo $x;
			echo "<br />";
			$indexVIS = $x;
			$BooleanArray[15] = true;
		}
		
		
	
	
	}
	while(!is_numeric($tok)){
		$tok = strtok(" \n\t");
	}
	
		
	
	while ($tok !== false) {
		$dataBuffer = array(); //store values based on their observed positions in the file.
		
		$dataBuffer[] = $tok;
		
		for($x = 1; $x < $indexBreak; $x++){
			$tok = strtok(" \n\t");
			$dataBuffer[] = $tok; 
		}
		
		if($BooleanArray[0]){
			$tempYear = $dataBuffer[$indexYear];
		}
		else{
			$tempYear = 0;
		}
		if($BooleanArray[1]){
			$tempMonth = $dataBuffer[$indexMonth];
			
		}
		else{
			$tempMonth = 0;
		}
		if($BooleanArray[2]){
			$tempDay = $dataBuffer[$indexDay];
		}
		else{
			$tempDay = 0;
		}
		if($BooleanArray[3]){
			$tempHour = $dataBuffer[$indexHour];
		}
		else{
			$tempHour = 0;
		}
		if($BooleanArray[4]){
			$tempMinute = $dataBuffer[$indexMinute];
		}
		else{
			$tempMinute = 0;
		}
		if($BooleanArray[5]){
			$tempWDIR = $dataBuffer[$indexWDIR];
		}
		else{
			$tempWDIR = 0;
		}
		
		if($BooleanArray[6]){
			$tempWSPD = $dataBuffer[$indexWSPD];
		}
		else{
			$tempWSPD = 0;
		}
		if($BooleanArray[7]){
			$tempGST = $dataBuffer[$indexGST];
		}
		else{
			$tempGST = 0;
		}
		if($BooleanArray[8]){
			$tempWVHT = $dataBuffer[$indexWVHT];
		}
		else{
			$tempWVHT = 0;
		}
		if($BooleanArray[9]){
			$tempDPD = $dataBuffer[$indexDPD];
		}
		else{
			$tempDPD = 0;
		}
		if($BooleanArray[10]){
			$tempAPD = $dataBuffer[$indexAPD];
		}
		else{
			$tempAPD = 0;
		}
		
		if($BooleanArray[11]){
			$tempMWD = $dataBuffer[$indexMWD];
		}
		else{
			$tempMWD = 0;
		}
		
		if($BooleanArray[12]){
			$tempATMP = $dataBuffer[$indexATMP];
		}
		else{
			$tempATMP = 0;
		}
		if($BooleanArray[13]){
			$tempWTMP = $dataBuffer[$indexWTMP];
		}
		else{
			$tempWTMP = 0;
		}
		if($BooleanArray[14]){
			$tempDEWP = $dataBuffer[$indexDEWP];
		}
		else{
			$tempDEWP = 0;
		}
		if($BooleanArray[15]){
			$tempVIS = $dataBuffer[$indexVIS];
		}
		else{
			$tempVIS = 0;
		}
		
		
		
			echo $tempYear;
			echo " : ";
			echo $tempMonth;
			echo " : ";
			echo $tempDay;
			echo " : ";
			echo $tempHour;
			echo "<br />";
		if(strlen($tempYear) < 3){
			$year = substr($year, -2, 2);

		}
		
		if($tempYear == $year
		&& $tempMonth == $month
		&& $tempDay == $day
		&& ($tempHour >= $hour-1 && $tempHour <= $hour+1)){
			if(!mysqli_query($link, 
			"INSERT INTO WEATHER VALUES (
			'$station', '$tempMonth/$tempDay/$tempYear', '$tempHour:$tempMinute', $retrievedLat, '$latdir', $retrievedLon, '$londir',
			0.0, $tempWDIR, $tempWSPD, $tempGST, $tempWVHT, $tempDPD, $tempAPD, $tempMWD, $tempATMP, $tempWTMP, $tempDEWP,
			$tempVIS)"
			
			
			)){
						echo "error:";
			}		
		//output to database and to file
		$file = "historical$station$tempYear$tempMonth$tempDay$tempHour$retrievedLat$latdir$retrievedLon$londir.txt";
		
		$person = "'$station', '$tempMonth/$tempDay/$tempYear', '$tempHour:$tempMinute', $retrievedLat, '$latdir', $retrievedLon,'$londir',0.0, $tempWDIR, $tempWSPD, $tempGST, $tempWVHT, $tempDPD, $tempAPD, $tempMWD, $tempATMP, $tempWTMP, $tempDEWP, $tempVIS\n";
		
		file_put_contents($file, $person, FILE_APPEND | LOCK_EX);
		
		}
		$tok = strtok(" ");
	}

}

?>
 </body>
</html>